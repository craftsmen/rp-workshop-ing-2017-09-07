const Rx = require('rxjs/Rx');

exports.sample = function(stream) {
	return Rx.Observable.interval(500).zip(stream, (a, b) => b);
}

exports.isPrime = function(n) {
	if (n < 1) {
		return false;
	}
	if (n < 4) {
		return true;
	}
	if (n % 2 == 0) {
		return false;
	}
	const sqrt = Math.sqrt(n);
	const sqrtCeiling = Math.ceil(sqrt);

	for (let i = 3; i <= sqrtCeiling; i += 2) {
		if (n % i == 0) {
			return false;
		}
	}
	return true;
}

const MIN_NORMAL = Math.pow(2, -1022);

exports.almostEqual = function(a, b, epsilon = 0.00001) {
	if (a === b) {
		return true;
	}
	
	const difference = Math.abs(a - b);

	if (a === 0 || b === 0 || difference < MIN_NORMAL) {
		return difference < epsilon * MIN_NORMAL;
	}

	return difference / (Math.abs(a) + Math.abs(b)) < epsilon
}

exports.consoleSupportsUnicode = function() {
	if (process.platform !== 'win32') {
		return true;
	}

	const consoleHints = [
		/^ConEmu/i,
		/^VSCODE_/i
	]

	return Object.keys(process.env).some((envKey) => consoleHints.some((hint) => hint.test(envKey)));
}
