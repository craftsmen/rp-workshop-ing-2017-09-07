const { almostEqual } = require('../../util/utils');

module.exports = class Rectangle {

	constructor(name, width, height) {
		this.name = name;
		this.width = width;
		this.height = height;
	}

	calculateArea() {
		return this.width * this.height;
	}

	calculateCircumference() {
		return 2 * this.width * this.height;
	}

	getName() {
		return this.name;
	}

	equals(other) {
		return (
			other instanceof Rectangle &&
			this.name === other.name &&
			almostEqual(this.width, other.width) &&
			almostEqual(this.height, other.height)
		);
	}

	toString() {
		return `Rectangle(name='${this.name}', width=${this.width}, height=${this.height})`;
	}
	
}
